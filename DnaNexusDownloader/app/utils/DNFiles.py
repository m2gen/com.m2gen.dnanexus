import pandas 	as pd
import datetime as dt


class DNFiles:

	def __init__(self, dxpy, projname):
		self.dxpy 	  = dxpy
		self.projname = projname


	def get_all_projects(self):
		#get a dict of all the projects and ids the user has access

	    #get more meta using describe option (which will include project name)
	    rg = list(self.dxpy.find_projects(level='VIEW', describe=True))

	    #get the names and ids
	    pids_u = [ r['describe']['id'] for r in rg ]
	    pnames_u = [ r['describe']['name'] for r in rg ]

	    #convert to ascii
	    pids = [ p.encode( 'ascii', 'ignore' )  for p in pids_u ]
	    pnames = [ p.encode( 'ascii', 'ignore' )  for p in pnames_u ]

	    #create dict of pids: pnames
	    pdict = dict( zip( pnames, pids ) )

	    return pdict
